namespace TwijgIT.OptionResult.Test {
  using System;

  using Xunit;

  using static Option;

  public class NoneTests {
    [Theory]
    [InlineData(0)]
    [InlineData(1)]
    public void Index(int index) {
      Assert.Throws<IndexOutOfRangeException>(() => None<bool>()[index]);
    }

    [Fact]
    public void Count() {
      Assert.Equal(0, None<bool>().Count);
    }

    [Fact]
    public void DefaultUnwrap() {
      Assert.Throws<InvalidOperationException>(() => default(None<bool>).Unwrap());
    }

    [Fact]
    public void IsNone() {
      Assert.True(None<bool>().IsNone);
    }

    [Fact]
    public void IsSome() {
      Assert.False(None<bool>().IsSome);
    }

    [Fact]
    public void String() {
      Assert.Equal("None", None<bool>().ToString());
    }
  }
}
