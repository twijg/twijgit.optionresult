namespace TwijgIT.OptionResult.Test {
  using System;
  using System.Collections.Generic;
  using System.Linq;

  using JetBrains.Annotations;

  using Xunit;

  using static Option;

  using static Result;

  public class EnumerableTests {
    [Theory]
    [MemberData(nameof(ToOptionData))]
    public void ToOption(IEnumerable<bool> enumerable, IOption<bool> expected) {
      Assert.Equal(expected, enumerable.ToOption());
    }

    [UsedImplicitly]
    public static IEnumerable<object[]> ToOptionData { get; } = new[] {
      new object[] { None<bool>(), None<bool>() }, new object[] { Some(false), Some(false) },
      new object[] { Some(true), Some(true) }, new object[] { new bool[0], None<bool>() },
      new object[] { new[] { false }, Some(false) }, new object[] { new[] { true }, Some(true) }
    };

    [Theory]
    [MemberData(nameof(ToOptionResultData))]
    public void ToOptionResult(IEnumerable<bool> enumerable, IResult<IOption<bool>, MultipleElementsError> expected) {
      Assert.Equal(expected, enumerable.ToOptionResult());
    }

    [UsedImplicitly]
    public static IEnumerable<object[]> ToOptionResultData { get; } = new[] {
      new object[] { None<bool>(), Ok<IOption<bool>, MultipleElementsError>(None<bool>()) },
      new object[] { Some(false), Ok<IOption<bool>, MultipleElementsError>(Some(false)) },
      new object[] { Some(true), Ok<IOption<bool>, MultipleElementsError>(Some(true)) },
      new object[] { new bool[0], Ok<IOption<bool>, MultipleElementsError>(None<bool>()) },
      new object[] { new[] { false }, Ok<IOption<bool>, MultipleElementsError>(Some(false)) },
      new object[] { new[] { true }, Ok<IOption<bool>, MultipleElementsError>(Some(true)) },
      new object[] { new[] { true, false }, Error<IOption<bool>, MultipleElementsError>(default) }
    };

    [Theory]
    [MemberData(nameof(ToSingleResultData))]
    public void ToSingleResult(IEnumerable<bool> enumerable, IResult<bool, ISingleError> expected) {
      Assert.Equal(expected, enumerable.ToSingleResult());
    }

    [UsedImplicitly]
    public static IEnumerable<object[]> ToSingleResultData { get; } = new[] {
      new object[] { None<bool>(), Error<bool, ISingleError>(default(NoElementError)) },
      new object[] { Some(false), Ok<bool, ISingleError>(false) },
      new object[] { Some(true), Ok<bool, ISingleError>(true) },
      new object[] { new bool[0], Error<bool, ISingleError>(default(NoElementError)) },
      new object[] { new[] { false }, Ok<bool, ISingleError>(false) },
      new object[] { new[] { true }, Ok<bool, ISingleError>(true) },
      new object[] { new[] { true, false }, Error<bool, ISingleError>(default(MultipleElementsError)) }
    };

    [Theory]
    [MemberData(nameof(SelectOptionData))]
    public void SelectOption(
      IEnumerable<bool> enumerable,
      Func<bool, IEnumerable<int>> selector,
      IOption<int> expected) {
      Assert.Equal(expected, enumerable.SelectOption(selector));
    }

    [UsedImplicitly]
    public static IEnumerable<object[]> SelectOptionData { get; } = new[] {
      new object[] { None<bool>(), new Func<bool, IEnumerable<int>>(b => new[] { 0 }), None<int>() },
      new object[] { Some(true), new Func<bool, IEnumerable<int>>(b => new int[0]), None<int>() },
      new object[] { Some(false), new Func<bool, IEnumerable<int>>(b => new[] { b ? 1 : 0 }), Some(0) },
      new object[] { Some(true), new Func<bool, IEnumerable<int>>(b => new[] { b ? 1 : 0 }), Some(1) }
    };

    [Theory]
    [MemberData(nameof(OrNullData))]
    public void FirstOrNull(IReadOnlyList<bool> enumerable) {
      var facade = enumerable.BehindFacade();
      switch (enumerable.Count) {
        case 0:
          Assert.Null(enumerable.FirstOrNull());
          Assert.Null(facade.FirstOrNull());
          break;
        default:
          Assert.Equal(enumerable[0], enumerable.FirstOrNull());
          Assert.Equal(enumerable[0], facade.FirstOrNull());
          break;
      }
    }

    [Theory]
    [MemberData(nameof(OrNullData))]
    public void SingleOrNull(IReadOnlyList<bool> enumerable) {
      var facade = enumerable.BehindFacade();
      switch (enumerable.Count) {
        case 0:
          Assert.Null(enumerable.SingleOrNull());
          Assert.Null(facade.SingleOrNull());
          break;
        case 1:
          Assert.Equal(enumerable[0], enumerable.SingleOrNull());
          Assert.Equal(enumerable[0], facade.SingleOrNull());
          break;
        default:
          Assert.Throws<InvalidOperationException>(() => enumerable.SingleOrNull());
          Assert.Throws<InvalidOperationException>(() => facade.SingleOrNull());
          break;
      }
    }

    [UsedImplicitly]
    public static IEnumerable<object[]> OrNullData { get; } = new[] {
      new object[] { new bool[0] },
      new object[] { new[] { true } },
      new object[] { new[] { false } },
      new object[] { new[] { false, true } }
    };

    [Fact]
    public void SelectOptionMulti() {
      Assert.Throws<InvalidOperationException>(() => Enumerable.Range(0, 2).SelectOption(i => Enumerable.Range(0, 1)));
    }

    [Fact]
    public void ToOptionMulti() {
      Assert.Throws<InvalidOperationException>(Enumerable.Range(0, 2).ToOption);
    }
  }
}
