namespace TwijgIT.OptionResult.Test {
  using System.Collections;
  using System.Collections.Generic;

  internal class EnumerableFacade<T> : IEnumerable<T> {
    private readonly IEnumerable<T> enumerable;

    public EnumerableFacade(IEnumerable<T> enumerable) {
      this.enumerable = enumerable;
    }

    IEnumerator<T> IEnumerable<T>.GetEnumerator() {
      return this.enumerable.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator() {
      return ((IEnumerable)this.enumerable).GetEnumerator();
    }
  }

  internal static class EnumerableFacadeExtensions {
    public static IEnumerable<T> BehindFacade<T>(this IEnumerable<T> enumerable) {
      return new EnumerableFacade<T>(enumerable);
    }
  }
}
