namespace TwijgIT.OptionResult.Test {
  using System;

  using Xunit;

  using static Result;

  public class ErrorTests {
    [Theory]
    [InlineData("Bummer")]
    [InlineData("Error")]
    public void Uncover(string error) {
      Assert.Equal(error, Error<bool, string>(error).Uncover());
    }

    [Theory]
    [InlineData(0)]
    [InlineData(1)]
    public void Index(int index) {
      Assert.Throws<IndexOutOfRangeException>(() => Error<bool, string>("Bummer")[index]);
    }

    [Fact]
    public void Count() {
      Assert.Equal(0, Error<bool, string>("Bummer").Count);
    }

    [Fact]
    public void Deconstruct() {
      Assert.True(Error<bool, string>("Bummer") is Error<bool, string>(var bummer) && bummer == "Bummer");
      Assert.True(Error<bool, string>("Bummer") is Error<bool, string>("Bummer"));
    }

    [Fact]
    public void DefaultUncover() {
      Assert.Throws<InvalidOperationException>(() => default(Error<bool, string>).Uncover());
    }

    [Fact]
    public void ImplicitConversion() {
      var error = (Error<bool, string>)Error<bool, string>("Bummer");
      Assert.Equal("Bummer", error);
    }

    [Fact]
    public void IsError() {
      Assert.True(Error<bool, string>("Bummer").IsError);
    }

    [Fact]
    public void IsOk() {
      Assert.False(Error<bool, string>("Bummer").IsOk);
    }

    [Fact]
    public void MultipleElements() {
      Assert.True(new MultipleElementsError().Equals(new MultipleElementsError()));
    }

    [Fact]
    public void NoElement() {
      Assert.True(new NoElementError().Equals(new NoElementError()));
    }

    [Fact]
    public void String() {
      Assert.Equal("Bummer", Error<bool, string>("Bummer").ToString());
    }

    [Fact]
    public void Unwrap() {
      Assert.Throws<InvalidOperationException>(() => Error<bool, string>("Bummer").Unwrap());
    }
  }
}
