namespace TwijgIT.OptionResult.Test {
  using System;
  using System.Collections.Generic;

  using JetBrains.Annotations;

  using Xunit;

  using static Result;

  public class ResultTests {
    [UsedImplicitly]
    public static IEnumerable<object[]> SelectOkData { get; } = new[] {
      new object[] { Error<bool, string>("bummer"), new Func<bool, int>(b => b ? 1 : 0), Error<int, string>("bummer") },
      new object[] { Ok<bool, string>(false), new Func<bool, int>(b => b ? 1 : 0), Ok<int, string>(0) },
      new object[] { Ok<bool, string>(true), new Func<bool, int>(b => b ? 1 : 0), Ok<int, string>(1) }
    };

    [UsedImplicitly]
    public static IEnumerable<object[]> AndResultData { get; } = new[] {
      new object[] {
        Error<bool, string>("bummer"), new Func<bool, IResult<int, string>>(b => Ok<int, string>(b ? 1 : 0)),
        Error<int, string>("bummer")
      },
      new object[] {
        Ok<bool, string>(false), new Func<bool, IResult<int, string>>(b => Error<int, string>(b.ToString())),
        Error<int, string>("False")
      },
      new object[] {
        Ok<bool, string>(false), new Func<bool, IResult<int, string>>(b => Ok<int, string>(b ? 1 : 0)),
        Ok<int, string>(0)
      },
      new object[] {
        Ok<bool, string>(true), new Func<bool, IResult<int, string>>(b => Ok<int, string>(b ? 1 : 0)),
        Ok<int, string>(1)
      }
    };

    [Theory]
    [MemberData(nameof(SelectOkData))]
    public void SelectOk(IResult<bool, string> result, Func<bool, int> func, IResult<int, string> expected) {
      Assert.Equal(expected, result.SelectOk(func));
    }

    [Theory]
    [MemberData(nameof(AndResultData))]
    public void AndResult(
      IResult<bool, string> result,
      Func<bool, IResult<int, string>> func,
      IResult<int, string> expected) {
      Assert.Equal(expected, result.And(func));
    }

    [Theory]
    [InlineData(false, 0)]
    [InlineData(true, 1)]
    public void SelectError(bool error0, int error1) {
      Assert.Equal(Error<bool, int>(error1), Error<bool, bool>(error0).SelectError(b => b ? 1 : 0));
    }
  }
}
