namespace TwijgIT.OptionResult.Test {
  using System;

  using Xunit;

  using static Option;

  public class SomeTests {
    [Theory]
    [InlineData(false)]
    [InlineData(true)]
    public void Index0(bool value) {
      Assert.Equal(value, Some(value)[0]);
    }

    [Fact]
    public void Count() {
      Assert.Equal(1, Some(false).Count);
    }

    [Fact]
    public void Deconstruct() {
      var someFive = Some(5);
      Assert.IsType<Some<int>>(someFive);
      switch (someFive) {
        case Some<int> (var five):
          Assert.Equal(5, five);
          break;
      }
    }

    [Fact]
    public void DefaultUnwrap() {
      Assert.Throws<InvalidOperationException>(() => default(Some<bool>).Unwrap());
    }

    [Fact]
    public void ImplicitConversion() {
      Some<bool> some = true;
      Assert.True(some);
    }

    [Fact]
    public void Index1() {
      Assert.Throws<IndexOutOfRangeException>(() => Some(true)[1]);
    }

    [Fact]
    public void IsNone() {
      Assert.False(Some(false).IsNone);
    }

    [Fact]
    public void IsSome() {
      Assert.True(Some(false).IsSome);
    }

    [Fact]
    public void String() {
      Assert.Equal(true.ToString(), Some(true).ToString());
    }
  }
}
