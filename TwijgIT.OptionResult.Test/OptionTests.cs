namespace TwijgIT.OptionResult.Test {
  using System;
  using System.Collections.Generic;

  using JetBrains.Annotations;

  using Xunit;

  using static Option;

  using static Result;

  public class OptionTests {
    [Theory]
    [MemberData(nameof(UnwrapOrLiteralData))]
    public void UnwrapOrLiteral(IOption<bool> option, bool literal, bool expected) {
      Assert.Equal(expected, option.UnwrapOr(literal));
    }

    [UsedImplicitly]
    public static IEnumerable<object[]> UnwrapOrLiteralData { get; } = new[] {
      new object[] { None<bool>(), false, false }, new object[] { None<bool>(), true, true },
      new object[] { Some(true), false, true }, new object[] { Some(false), true, false }
    };

    [Theory]
    [MemberData(nameof(UnwrapOrFuncData))]
    public void UnwrapOrFunc(IOption<bool> option, Func<bool> func, bool expected) {
      Assert.Equal(expected, option.UnwrapOr(func));
    }

    [UsedImplicitly]
    public static IEnumerable<object[]> UnwrapOrFuncData { get; } = new[] {
      new object[] { None<bool>(), new Func<bool>(() => false), false },
      new object[] { None<bool>(), new Func<bool>(() => true), true },
      new object[] { Some(true), new Func<bool>(() => false), true },
      new object[] { Some(false), new Func<bool>(() => true), false }
    };

    [Theory]
    [MemberData(nameof(AndLiteralData))]
    public void AndLiteral(IOption<bool> option, IOption<bool> literal, IOption<bool> expected) {
      Assert.Equal(expected, option.And(literal));
    }

    [UsedImplicitly]
    public static IEnumerable<object[]> AndLiteralData { get; } = new[] {
      new object[] { None<bool>(), Some(false), None<bool>() }, new object[] { None<bool>(), Some(true), None<bool>() },
      new object[] { Some(false), Some(true), Some(true) }, new object[] { Some(true), Some(false), Some(false) }
    };

    [Theory]
    [MemberData(nameof(AndFuncData))]
    public void AndFunc(IOption<bool> option, Func<IOption<bool>> func, IOption<bool> expected) {
      Assert.Equal(expected, option.And(func));
    }

    [UsedImplicitly]
    public static IEnumerable<object[]> AndFuncData { get; } = new[] {
      new object[] { None<bool>(), new Func<IOption<bool>>(() => Some(true)), None<bool>() },
      new object[] { Some(false), new Func<IOption<bool>>(() => Some(true)), Some(true) },
      new object[] { Some(true), new Func<IOption<bool>>(() => Some(false)), Some(false) },
      new object[] { Some(true), new Func<IOption<bool>>(() => default(None<bool>)), None<bool>() }
    };

    [Theory]
    [MemberData(nameof(SelectData))]
    public void Select(IOption<bool> option, Func<bool, int> func, IOption<int> expected) {
      Assert.Equal(expected, option.Select(func));
    }

    [UsedImplicitly]
    public static IEnumerable<object[]> SelectData { get; } = new[] {
      new object[] { None<bool>(), new Func<bool, int>(b => b ? 1 : 0), None<int>() },
      new object[] { Some(false), new Func<bool, int>(b => b ? 1 : 0), Some(0) },
      new object[] { Some(true), new Func<bool, int>(b => b ? 1 : 0), Some(1) }
    };

    [Theory]
    [MemberData(nameof(OrConcatData))]
    public void OrConcat(IOption<string> option, IOption<string> alternative, IOption<string> expected) {
      Assert.Equal(expected, option.Or(alternative));
    }

    [UsedImplicitly]
    public static IEnumerable<object[]> OrConcatData { get; } = new[] {
      new object[] { None<string>(), Some("a"), Some("a") }, new object[] { Some("a"), None<string>(), Some("a") },
      new object[] { Some("a"), Some("b"), Some("a") }, new object[] { None<string>(), None<string>(), None<string>() }
    };

    [Theory]
    [MemberData(nameof(OrLiteralData))]
    public void OrLiteral(IOption<bool> option, string literal, IResult<bool, string> expected) {
      Assert.Equal(expected, option.Or(literal));
    }

    [UsedImplicitly]
    public static IEnumerable<object[]> OrLiteralData { get; } = new[] {
      new object[] { None<bool>(), "a", Error<bool, string>("a") },
      new object[] { None<bool>(), "b", Error<bool, string>("b") },
      new object[] { Some(true), "a", Ok<bool, string>(true) }
    };

    [Theory]
    [MemberData(nameof(OrFuncData))]
    public void OrFunc(IOption<bool> option, Func<string> func, IResult<bool, string> expected) {
      Assert.Equal(expected, option.Or(func));
    }

    [UsedImplicitly]
    public static IEnumerable<object[]> OrFuncData { get; } = new[] {
      new object[] { None<bool>(), new Func<string>(() => "a"), Error<bool, string>("a") },
      new object[] { None<bool>(), new Func<string>(() => "b"), Error<bool, string>("b") },
      new object[] { Some(true), new Func<string>(() => "a"), Ok<bool, string>(true) }
    };

    [Theory]
    [MemberData(nameof(ForEachData))]
    public void ForEach(IOption<bool> option, bool expected) {
      var local = false;
      option.ForEach(b => local = b);
      Assert.Equal(local, expected);
    }

    [UsedImplicitly]
    public static IEnumerable<object[]> ForEachData { get; } = new[] {
      new object[] { Some(false), false }, new object[] { Some(true), true }, new object[] { None<bool>(), false }
    };

    [Fact]
    public void NoneTest() {
      Assert.IsType<None<bool>>(None<bool>());
    }

    [Fact]
    public void SomeTest() {
      Assert.IsType<Some<bool>>(Some(true));
    }
  }
}
