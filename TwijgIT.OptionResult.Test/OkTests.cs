namespace TwijgIT.OptionResult.Test {
  using System;

  using Xunit;

  using static Result;

  public class OkTests {
    [Theory]
    [InlineData(false)]
    [InlineData(true)]
    public void Index0(bool value) {
      Assert.Equal(value, Ok<bool, string>(value)[0]);
    }

    [Fact]
    public void Count() {
      Assert.Equal(1, Ok<bool, string>(true).Count);
    }

    [Fact]
    public void Deconstruct() {
      Assert.True(Ok<bool, string>(true) is Ok<bool, string>(var ok) && ok);
      Assert.True(Ok<bool, string>(true) is Ok<bool, string>(true));
    }

    [Fact]
    public void DefaultUnwrap() {
      Assert.Throws<InvalidOperationException>(() => default(Ok<bool, string>).Unwrap());
    }

    [Fact]
    public void ImplicitConversion() {
      Ok<bool, string> ok = true;
      Assert.True(ok);
    }

    [Fact]
    public void Index1() {
      Assert.Throws<IndexOutOfRangeException>(() => Ok<bool, string>(true)[1]);
    }

    [Fact]
    public void IsError() {
      Assert.False(Ok<bool, string>(true).IsError);
    }

    [Fact]
    public void IsOk() {
      Assert.True(Ok<bool, string>(true).IsOk);
    }

    [Fact]
    public void String() {
      Assert.Equal(true.ToString(), Ok<bool, string>(true).ToString());
    }

    [Fact]
    public void Uncover() {
      Assert.Throws<InvalidOperationException>(() => default(Ok<bool, string>).Uncover());
    }
  }
}
