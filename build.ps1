param(
  [string]$packageVersion = "1.0.0.0"
)

$ErrorActionPreference = "Stop"

$currentVersion = if(Test-Path env:PackageVersion) {
  Get-Content env:PackageVersion
} else {
  $packageVersion
}

function CheckLastExitCode {
  param ([int[]]$SuccessCodes = @(0))
  if ($SuccessCodes -NotContains $LastExitCode) {
    $msg = "Exit code: $LastExitCode"
    throw $msg
  }
}

Write-Host "Testing $currentVersion ..."
dotnet test -f net452 -c Release TwijgIT.OptionResult.Test/TwijgIT.OptionResult.Test.csproj
CheckLastExitCode

Write-Host "Build..."
dotnet build -c Release

Write-Host "Ready!"
