[![twijg-public MyGet Build Status](https://www.myget.org/BuildSource/Badge/twijg-public?identifier=4be68f35-ec3f-495d-947c-ceb826afcedc)](https://www.myget.org/)

# TwijgIT.OptionResult

Interfaces `IOption<T>` and `IResult<TOk, TError>` with struct implementations.

## IOption

An option is like a nullable, only it can be applied to any type (either reference or value) and can be nested.

## IResult

A result can be in one of two states: either the operation was succesful and an Ok value is returned, or the operation failed and the specification is returned in an Error value.