namespace TwijgIT.OptionResult {
  using JetBrains.Annotations;

  /// <summary>
  /// Marker interface for errors converting an enumerable to a single value.
  /// </summary>
  [PublicAPI]
  public interface ISingleError { }
}
