namespace TwijgIT.OptionResult {
  using System;
  using System.Diagnostics.CodeAnalysis;

  using JetBrains.Annotations;

  /// <summary>
  /// Struct with one possible value.
  /// </summary>
  [PublicAPI]
  public struct Unit : IEquatable<Unit> {
    /// <inheritdoc />
    public bool Equals(Unit other) {
      return true;
    }

    /// <inheritdoc />
    public override bool Equals(object obj) {
      return obj is Unit;
    }

    /// <inheritdoc />
    public override int GetHashCode() {
      return 0;
    }

    /// <remarks />
    [SuppressMessage("ReSharper", "UnusedParameter.Global")]
    public static bool operator ==(Unit left, Unit right) {
      return true;
    }

    /// <remarks />
    [SuppressMessage("ReSharper", "UnusedParameter.Global")]
    public static bool operator !=(Unit left, Unit right) {
      return false;
    }

    /// <inheritdoc />
    public override string ToString() {
      return "()";
    }
  }
}
