namespace TwijgIT.OptionResult {
  using System;
  using System.Collections;
  using System.Collections.Generic;
  using System.Linq;

  using JetBrains.Annotations;

  /// <summary>
  /// The error struct.
  /// </summary>
  /// <typeparam name="TOk">The OK type.</typeparam>
  /// <typeparam name="TError">The error type.</typeparam>
  /// <remarks>A default instance of this struct should not be created.</remarks>
  [PublicAPI]
  public struct Error<TOk, TError> : IResult<TOk, TError> where TOk : notnull where TError : notnull {
    private readonly TError wrapped;

    internal Error(TError error) {
      this.wrapped = error;
      this.IsError = true;
    }

    /// <summary>
    /// Gets enumerator.
    /// </summary>
    /// <returns>An empty enumerator.</returns>
    public IEnumerator<TOk> GetEnumerator() {
      return Enumerable.Empty<TOk>().GetEnumerator();
    }

    /// <summary>
    /// Gets enumerator.
    /// </summary>
    /// <returns>An empty enumerator.</returns>
    IEnumerator IEnumerable.GetEnumerator() {
      return this.GetEnumerator();
    }

    /// <summary>
    /// Gets the number of elements (always 0).
    /// </summary>
    public int Count => 0;

    /// <summary>
    /// Gets the indexed value.
    /// </summary>
    /// <param name="index">The index.</param>
    /// <exception cref="IndexOutOfRangeException">Always thrown.</exception>
    public TOk this[int index] =>
      throw new IndexOutOfRangeException($"There are no valid values for ${nameof(index)}.");

    /// <summary>
    /// Determines equality to other result.
    /// </summary>
    /// <param name="other">The other result.</param>
    /// <returns>Where other result is equal to current instance.</returns>
    public bool Equals(IResult<TOk, TError> other) {
      return other?.IsError == true && Equals(this.Uncover(), other.Uncover());
    }

    /// <summary>
    /// Gets a value indicating whether this result is Ok.
    /// </summary>
    public bool IsOk => false;

    /// <summary>
    /// Gets a value indicating whether this result is Error.
    /// </summary>
    public bool IsError { get; }

    /// <summary>
    /// Unwraps the Ok value.
    /// </summary>
    /// <returns>Nothing, always throws an exception.</returns>
    /// <exception cref="InvalidOperationException">Always thrown.</exception>
    public TOk Unwrap() {
      throw new InvalidOperationException($"Cannot {nameof(this.Unwrap)} {nameof(Error<TOk, TError>)}.");
    }

    /// <summary>
    /// Uncovers the Error value.
    /// </summary>
    /// <returns>The Error value.</returns>
    /// <exception cref="InvalidOperationException">When called on a default instance.</exception>
    public TError Uncover() {
      return this.IsError
               ? this.wrapped
               : throw new InvalidOperationException(
                   $"Cannot {nameof(this.Uncover)} default value of {nameof(Error<TOk, TError>)}.");
    }

    /// <summary>
    /// Gets a string representation of the current instance.
    /// </summary>
    /// <returns>A string representation of the current instance.</returns>
    public override string ToString() {
      return this.Uncover().ToString();
    }

    /// <summary>
    /// Deconstructs the Error.
    /// </summary>
    /// <param name="error">The error.</param>
    public void Deconstruct(out TError error) {
      error = this.Uncover();
    }

    /// <summary>
    /// Implicitly converts an instance to the error value.
    /// </summary>
    /// <param name="error">The instance.</param>
    /// <returns>The error value.</returns>
    public static implicit operator TError(Error<TOk, TError> error) {
      return error.Uncover();
    }
  }
}
