namespace TwijgIT.OptionResult {
  using System;

  using JetBrains.Annotations;

  /// <summary>
  /// Utility methods for IOption&lt;T&gt;
  /// </summary>
  [PublicAPI]
  public static class Option {
    /// <summary>
    /// Creates a None value.
    /// </summary>
    /// <typeparam name="T">The value type.</typeparam>
    /// <returns>The None value.</returns>
    [NotNull]
    [Pure]
    public static IOption<T> None<T>() where T : notnull {
      return new None<T>();
    }

    /// <summary>
    /// Creates a Some value.
    /// </summary>
    /// <param name="value">The value.</param>
    /// <typeparam name="T">The value type.</typeparam>
    /// <returns>The Some value.</returns>
    [NotNull]
    [Pure]
    public static IOption<T> Some<T>([NotNull] T value) where T : notnull {
      return new Some<T>(value);
    }

    /// <summary>
    /// Unwraps or gives default value.
    /// </summary>
    /// <param name="option">The option.</param>
    /// <param name="defaultValue">The default value.</param>
    /// <typeparam name="T">The value type.</typeparam>
    /// <returns>The option value if present or the default value otherwise.</returns>
    [ContractAnnotation("defaultValue:notnull => notnull")]
    [Pure]
    public static T UnwrapOr<T>([ItemNotNull] [NotNull] this IOption<T> option, T defaultValue) where T : notnull {
      return option switch {
        Some<T> (var value) => value,
        _ => defaultValue
      };
    }

    /// <summary>
    /// Unwraps or generates alternative value.
    /// </summary>
    /// <param name="option">The option.</param>
    /// <param name="func">Function to generate alternative value.</param>
    /// <typeparam name="T">The value type.</typeparam>
    /// <returns>The option value if present or the generated alternative value otherwise.</returns>
    [NotNull]
    [Pure]
    public static T UnwrapOr<T>([ItemNotNull] [NotNull] this IOption<T> option, [NotNull] Func<T> func)
      where T : notnull {
      return option switch {
        Some<T> (var value) => value,
        _ => func()
      };
    }

    /// <summary>
    /// Chains two options.
    /// </summary>
    /// <param name="option">The option.</param>
    /// <param name="chainOption">The chain option.</param>
    /// <typeparam name="T0">The option value type.</typeparam>
    /// <typeparam name="T1">The chain option value type.</typeparam>
    /// <returns>None if either of the options is None, the chainOption otherwise.</returns>
    [ItemNotNull]
    [NotNull]
    [Pure]
    public static IOption<T1> And<T0, T1>(
      [ItemNotNull] [NotNull] this IOption<T0> option,
      [ItemNotNull] [NotNull] IOption<T1> chainOption) where T0 : notnull where T1 : notnull {
      return option switch {
        Some<T0> _ => chainOption,
        _ => None<T1>()
      };
    }

    /// <summary>
    /// Chains two options.
    /// </summary>
    /// <param name="option">The option.</param>
    /// <param name="func">Function to generate the chain option.</param>
    /// <typeparam name="T0">The option value type.</typeparam>
    /// <typeparam name="T1">The chain option value type.</typeparam>
    /// <returns>None if either of the options is None, the chainOption otherwise.</returns>
    [ItemNotNull]
    [NotNull]
    [Pure]
    public static IOption<T1> And<T0, T1>(
      [ItemNotNull] [NotNull] this IOption<T0> option,
      [NotNull] Func<IOption<T1>> func) where T0 : notnull where T1 : notnull {
      return option switch {
        Some<T0> _ => func(),
        _ => None<T1>()
      };
    }

    /// <summary>
    /// Maps the value inside an option.
    /// </summary>
    /// <param name="option">The option.</param>
    /// <param name="func">Function to map the value.</param>
    /// <typeparam name="T0">The option value type.</typeparam>
    /// <typeparam name="T1">The return option value type.</typeparam>
    /// <returns>None if the options is None, a Some of the mapped value otherwise.</returns>
    [ItemNotNull]
    [NotNull]
    [Pure]
    public static IOption<T1> Select<T0, T1>(
      [ItemNotNull] [NotNull] this IOption<T0> option,
      [NotNull] Func<T0, T1> func) where T0 : notnull where T1 : notnull {
      return option switch {
        Some<T0> value => Some(func(value)),
        _ => None<T1>()
      };
    }

    /// <summary>
    /// Applies the OR operator to two options, preferring the first.
    /// </summary>
    /// <param name="option">The first option.</param>
    /// <param name="alternative">The alternative option.</param>
    /// <typeparam name="TValue">The option value type.</typeparam>
    /// <returns>Some of the first value found, None if both are None.</returns>
    [ItemNotNull]
    [NotNull]
    [Pure]
    public static IOption<TValue> Or<TValue>(
      [ItemNotNull] [NotNull] this IOption<TValue> option,
      [ItemNotNull] [NotNull] IOption<TValue> alternative) where TValue : notnull {
      return option.IsSome ? option : alternative;
    }

    /// <summary>
    /// Converts the option to a result.
    /// </summary>
    /// <param name="option">The option.</param>
    /// <param name="error">The error value.</param>
    /// <typeparam name="TOk">The option and Ok value type.</typeparam>
    /// <typeparam name="TError">The Error value type.</typeparam>
    /// <returns>An Ok if the option is Some, an Error otherwise.</returns>
    [ItemNotNull]
    [NotNull]
    [Pure]
    public static IResult<TOk, TError> Or<TOk, TError>(
      [ItemNotNull] [NotNull] this IOption<TOk> option,
      [NotNull] TError error) where TOk : notnull where TError : notnull {
      return option switch {
        Some<TOk> (var value) => new Ok<TOk, TError>(value),
        _ => Result.Error<TOk, TError>(error)
      };
    }

    /// <summary>
    /// Converts the option to a result.
    /// </summary>
    /// <param name="option">The option.</param>
    /// <param name="func">Function to generate the error value.</param>
    /// <typeparam name="TOk">The option and Ok value type.</typeparam>
    /// <typeparam name="TError">The Error value type.</typeparam>
    /// <returns>An Ok if the option is Some, an Error otherwise.</returns>
    [ItemNotNull]
    [NotNull]
    [Pure]
    public static IResult<TOk, TError> Or<TOk, TError>(
      [ItemNotNull] [NotNull] this IOption<TOk> option,
      [NotNull] Func<TError> func) where TOk : notnull where TError : notnull {
      return option switch {
        Some<TOk> (var value) => new Ok<TOk, TError>(value),
        _ => Result.Error<TOk, TError>(func())
      };
    }

    /// <summary>
    /// Executes action for value only if IsSome.
    /// </summary>
    /// <param name="option">The option.</param>
    /// <param name="action">The action.</param>
    /// <typeparam name="TValue">The option value type.</typeparam>
    public static void ForEach<TValue>(
      [ItemNotNull] [NotNull] this IOption<TValue> option,
      [NotNull] Action<TValue> action) where TValue : notnull {
      if (option is Some<TValue> (var value)) {
        action(value);
      }
    }

    /// <summary>
    /// Constructs an IOption from a nullable value.
    /// </summary>
    /// <param name="nullable">The nullable.</param>
    /// <typeparam name="TValue">The value type.</typeparam>
    /// <returns>The IOption.</returns>
    [NotNull]
    [Pure]
    public static IOption<TValue> ToOption<TValue>(this TValue? nullable) where TValue : struct {
      return nullable is {} value ? Some(value) : None<TValue>();
    }

    /// <summary>
    /// Constructs an IOption from a nullable value.
    /// </summary>
    /// <param name="nullable">The nullable.</param>
    /// <typeparam name="TValue">The value type.</typeparam>
    /// <returns>The IOption.</returns>
    [NotNull]
    [Pure]
    public static IOption<TValue> From<TValue>(TValue? nullable) where TValue : struct {
      return nullable is {} value ? Some(value) : None<TValue>();
    }

    /// <summary>
    /// Constructs an IOption from a nullable reference.
    /// </summary>
    /// <param name="reference">The reference.</param>
    /// <typeparam name="TValue">The value type.</typeparam>
    /// <returns>The IOption.</returns>
    [NotNull]
    [ItemNotNull]
    [Pure]
    public static IOption<TValue> From<TValue>(TValue? reference) where TValue : class {
      return reference is {} value ? Some(value) : None<TValue>();
    }
  }
}
