namespace TwijgIT.OptionResult {
  using System;
  using System.Collections;
  using System.Collections.Generic;
  using System.Linq;

  using JetBrains.Annotations;

  /// <summary>
  /// Option containing no value.
  /// </summary>
  /// <typeparam name="T">The value type.</typeparam>
  [PublicAPI]
  public struct None<T> : IOption<T> where T : notnull {
    /// <summary>
    /// Gets a value indicating whether there is a value.
    /// </summary>
    public bool IsSome => false;

    /// <summary>
    /// Gets a value indicating whether there is no value.
    /// </summary>
    public bool IsNone => true;

    /// <summary>
    /// Unwraps the value.
    /// </summary>
    /// <returns>The value.</returns>
    /// <exception cref="InvalidOperationException">Always thrown.</exception>
    public T Unwrap() {
      throw new InvalidOperationException($"Cannot {nameof(this.Unwrap)} {nameof(None<T>)}.");
    }

    /// <summary>
    /// Gets an empty enumerator.
    /// </summary>
    /// <returns>The enumerator.</returns>
    public IEnumerator<T> GetEnumerator() {
      return Enumerable.Empty<T>().GetEnumerator();
    }

    /// <summary>
    /// Gets an empty enumerator.
    /// </summary>
    /// <returns>The enumerator.</returns>
    IEnumerator IEnumerable.GetEnumerator() {
      return this.GetEnumerator();
    }

    /// <summary>
    /// Gets the number of values (always 0).
    /// </summary>
    public int Count => 0;

    /// <summary>
    /// Gets the indexed value.
    /// </summary>
    /// <param name="index">The index.</param>
    /// <exception cref="IndexOutOfRangeException">Always thrown.</exception>
    public T this[int index] => throw new IndexOutOfRangeException($"There are no valid values for ${nameof(index)}.");

    /// <summary>
    /// Determines equality to other option.
    /// </summary>
    /// <param name="other">The other option.</param>
    /// <returns>Where other option is equal to current instance.</returns>
    public bool Equals(IOption<T> other) {
      return other?.IsNone == true;
    }

    /// <summary>
    /// Gets a string representation of the current instance.
    /// </summary>
    /// <returns>A string representation of the current instance.</returns>
    public override string ToString() {
      return "None";
    }
  }
}
