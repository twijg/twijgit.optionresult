namespace TwijgIT.OptionResult {
  using System;
  using System.Collections.Generic;

  using JetBrains.Annotations;

  /// <summary>
  /// The option interface. Contains zero or one value.
  /// </summary>
  /// <typeparam name="T">The value type.</typeparam>
  [PublicAPI]
  public interface IOption<T> : IReadOnlyList<T>, IEquatable<IOption<T>> where T : notnull {
    /// <summary>
    /// Gets a value indicating whether there is a value.
    /// </summary>
    bool IsSome { get; }

    /// <summary>
    /// Gets a value indicating whether there is no value.
    /// </summary>
    bool IsNone { get; }

    /// <summary>
    /// Unwraps the value.
    /// </summary>
    /// <returns>The value.</returns>
    /// <exception cref="InvalidOperationException">If called on None.</exception>
    [NotNull]
    [Pure]
    T Unwrap();
  }
}
