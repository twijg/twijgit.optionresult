namespace TwijgIT.OptionResult {
  using System;
  using System.Collections.Generic;
  using System.Linq;

  using JetBrains.Annotations;

  /// <summary>
  /// Extensions for IEnumerable&lt;T&gt;
  /// </summary>
  [PublicAPI]
  public static class EnumerableExtensions {
    /// <summary>
    /// Converts enumerable to option.
    /// </summary>
    /// <param name="values">The enumerable.</param>
    /// <typeparam name="TValue">The value type.</typeparam>
    /// <returns>
    /// An option of the value type.
    /// </returns>
    /// <exception cref="InvalidOperationException">
    /// If enumerable contains multiple elements.
    /// </exception>
    [ItemNotNull]
    [NotNull]
    [Pure]
    public static IOption<TValue> ToOption<TValue>([NotNull] [ItemNotNull] this IEnumerable<TValue> values)
      where TValue : notnull {
      if (values is IOption<TValue> option) {
        return option;
      }

      using (var enumerator = values.GetEnumerator()) {
        if (!enumerator.MoveNext()) {
          return Option.None<TValue>();
        }

        var value = enumerator.Current;
        if (enumerator.MoveNext()) {
          throw new InvalidOperationException($"{nameof(IOption<TValue>)} cannot contain multiple values.");
        }

        // ReSharper disable once AssignNullToNotNullAttribute
        return Option.Some(value);
      }
    }

    /// <summary>
    /// Converts enumerable to result with optional value.
    /// </summary>
    /// <param name="values">The enumerable.</param>
    /// <typeparam name="TValue">The value type.</typeparam>
    /// <returns>
    /// An option of the value type if Ok, a MultipleElementsError if enumerable contains multiple elements.
    /// </returns>
    [ItemNotNull]
    [NotNull]
    [Pure]
    public static IResult<IOption<TValue>, MultipleElementsError> ToOptionResult<TValue>(
      [NotNull] [ItemNotNull] this IEnumerable<TValue> values) where TValue : notnull {
      if (values is IOption<TValue> option) {
        return Result.Ok<IOption<TValue>, MultipleElementsError>(option);
      }

      using (var enumerator = values.GetEnumerator()) {
        if (!enumerator.MoveNext()) {
          return Result.Ok<IOption<TValue>, MultipleElementsError>(Option.None<TValue>());
        }

        var value = enumerator.Current;
        if (enumerator.MoveNext()) {
          return Result.Error<IOption<TValue>, MultipleElementsError>(new MultipleElementsError());
        }

        // ReSharper disable once AssignNullToNotNullAttribute
        return Result.Ok<IOption<TValue>, MultipleElementsError>(Option.Some(value));
      }
    }

    /// <summary>
    /// Converts enumerable to result with one value.
    /// </summary>
    /// <param name="values">The enumerable.</param>
    /// <typeparam name="TValue">The value type.</typeparam>
    /// <returns>
    /// The single value if Ok,
    /// a NoElementError if enumerable contains no element,
    /// MultipleElementsError if enumerable contains multiple elements.
    /// </returns>
    [ItemNotNull]
    [NotNull]
    [Pure]
    public static IResult<TValue, ISingleError> ToSingleResult<TValue>(
      [NotNull] [ItemNotNull] this IEnumerable<TValue> values) where TValue : notnull {
      if (values is Some<TValue> option) {
        return Result.Ok<TValue, ISingleError>(option);
      }

      using (var enumerator = values.GetEnumerator()) {
        if (!enumerator.MoveNext()) {
          return Result.Error<TValue, ISingleError>(new NoElementError());
        }

        var value = enumerator.Current;
        if (enumerator.MoveNext()) {
          return Result.Error<TValue, ISingleError>(new MultipleElementsError());
        }

        // ReSharper disable once AssignNullToNotNullAttribute
        return Result.Ok<TValue, ISingleError>(value);
      }
    }

    /// <summary>
    /// Selects an option from an enumerable.
    /// </summary>
    /// <param name="values">The enumerable.</param>
    /// <param name="func">The selector function.</param>
    /// <typeparam name="TValue">The value type.</typeparam>
    /// <typeparam name="TResult">The result type.</typeparam>
    /// <returns>
    /// An option of the result type.
    /// </returns>
    /// <exception cref="InvalidOperationException">
    /// If the expansion of the enumerable contains multiple elements.
    /// </exception>
    [ItemNotNull]
    [NotNull]
    [Pure]
    public static IOption<TResult> SelectOption<TValue, TResult>(
      this IEnumerable<TValue> values,
      Func<TValue, IEnumerable<TResult>> func) where TResult : notnull {
      return values.SelectMany(func).ToOption();
    }

    /// <summary>
    /// Converts enumerable to nullable.
    /// </summary>
    /// <param name="values">The enumerable.</param>
    /// <typeparam name="TValue">The value type.</typeparam>
    /// <returns>
    /// An nullable of the value type.
    /// </returns>
    /// <exception cref="InvalidOperationException">
    /// If enumerable contains multiple elements.
    /// </exception>
    [Pure]
    public static TValue? SingleOrNull<TValue>([NotNull] this IEnumerable<TValue> values) where TValue : struct {
      switch (values) {
        case None<TValue> _:
          return null;
        case Some<TValue> some:
          return some.Unwrap();
        case ICollection<TValue> collection:
          switch (collection.Count) {
            case 0:
              return null;
            case 1 when collection is IReadOnlyList<TValue> list:
              return list[0];
            case 1:
              return collection.First();
            default:
              throw new InvalidOperationException($"{values} must not contain multiple values.");
          }
      }

      using (var enumerator = values.GetEnumerator()) {
        if (!enumerator.MoveNext()) {
          return null;
        }

        var value = enumerator.Current;
        if (enumerator.MoveNext()) {
          throw new InvalidOperationException($"{values} must not contain multiple values.");
        }

        return value;
      }
    }

    /// <summary>
    /// Converts enumerable to nullable.
    /// </summary>
    /// <param name="values">The enumerable.</param>
    /// <typeparam name="TValue">The value type.</typeparam>
    /// <returns>
    /// An nullable of the value type.
    /// </returns>
    [Pure]
    public static TValue? FirstOrNull<TValue>([NotNull] this IEnumerable<TValue> values) where TValue : struct {
      switch (values) {
        case None<TValue> _:
          return null;
        case Some<TValue> some:
          return some.Unwrap();
        case ICollection<TValue> collection:
          switch (collection.Count) {
            case 0:
              return null;
            case var _ when collection is IReadOnlyList<TValue> list:
              return list[0];
            default:
              return collection.First();
          }
      }

      using (var enumerator = values.GetEnumerator()) {
        return enumerator.MoveNext() ? enumerator.Current : (TValue?)null;
      }
    }
  }
}
