namespace TwijgIT.OptionResult {
  using System;

  using JetBrains.Annotations;

  /// <summary>
  /// Marker interface for multiple values error when converting an enumerable to an option or a single value.
  /// </summary>
  [PublicAPI]
  public struct MultipleElementsError : IEquatable<MultipleElementsError>, ISingleError {
    /// <summary>
    /// Determines equality to other MultipleElementsError.
    /// </summary>
    /// <param name="other">The other MultipleElementsError.</param>
    /// <returns>Always true.</returns>
    public bool Equals(MultipleElementsError other) {
      return true;
    }
  }
}
