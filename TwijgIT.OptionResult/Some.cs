namespace TwijgIT.OptionResult {
  using System;
  using System.Collections;
  using System.Collections.Generic;
  using System.Linq;

  using JetBrains.Annotations;

  /// <summary>
  /// Option containing a value.
  /// </summary>
  /// <typeparam name="T">The value type.</typeparam>
  /// <remarks>A default instance of this struct should not be created.</remarks>
  [PublicAPI]
  public struct Some<T> : IOption<T> where T : notnull {
    [NotNull] private readonly T wrapped;

    internal Some([NotNull] T value) {
      this.wrapped = value;
      this.IsSome = true;
    }

    /// <summary>
    /// Gets a value indicating whether there is a value.
    /// </summary>
    public bool IsSome { get; }

    /// <summary>
    /// Gets a value indicating whether there is no value.
    /// </summary>
    public bool IsNone => false;

    /// <summary>
    /// Unwraps the value.
    /// </summary>
    /// <returns>The value.</returns>
    /// <exception cref="InvalidOperationException">If called on a default value of Some.</exception>
    public T Unwrap() {
      return this.IsSome
               ? this.wrapped
               : throw new InvalidOperationException(
                   $"Cannot {nameof(this.Unwrap)} default value of {nameof(Some<T>)}.");
    }

    /// <summary>
    /// Deconstructs the Some.
    /// </summary>
    /// <param name="value">The value.</param>
    public void Deconstruct(out T value) {
      value = this.Unwrap();
    }

    /// <summary>
    /// Converts Some to the wrapped value.
    /// </summary>
    /// <param name="some">An instance of Some.</param>
    /// <returns>The wrapped value.</returns>
    public static implicit operator T(Some<T> some) {
      return some.Unwrap();
    }

    /// <summary>
    /// Wraps a value in the Some struct.
    /// </summary>
    /// <param name="value">The value.</param>
    /// <returns>An instance of Some wrapping the value.</returns>
    public static implicit operator Some<T>(T value) {
      return new Some<T>(value);
    }

    /// <summary>
    /// Gets an enumerator.
    /// </summary>
    /// <returns>The enumerator.</returns>
    public IEnumerator<T> GetEnumerator() {
      return Enumerable.Repeat(this.Unwrap(), 1).GetEnumerator();
    }

    /// <summary>
    /// Gets an enumerator.
    /// </summary>
    /// <returns>The enumerator.</returns>
    IEnumerator IEnumerable.GetEnumerator() {
      return this.GetEnumerator();
    }

    /// <summary>
    /// Gets the number of values (always 1).
    /// </summary>
    public int Count => 1;

    /// <summary>
    /// Gets the value by index.
    /// </summary>
    /// <exception cref="IndexOutOfRangeException">If index is not zero.</exception>
    [NotNull]
    public T this[int index] =>
      index == 0 ? this.Unwrap() : throw new IndexOutOfRangeException($"Value of ${nameof(index)} must be 0.");

    /// <summary>
    /// Determines equality to other option.
    /// </summary>
    /// <param name="other">The other option.</param>
    /// <returns>Where other option is equal to current instance.</returns>
    public bool Equals(IOption<T> other) {
      var thisValue = this.Unwrap();
      return other?.Any(otherValue => Equals(otherValue, thisValue)) == true;
    }

    /// <summary>
    /// Gets a string representation of the current instance.
    /// </summary>
    /// <returns>A string representation of the current instance.</returns>
    public override string ToString() {
      return this.Unwrap().ToString();
    }
  }
}
