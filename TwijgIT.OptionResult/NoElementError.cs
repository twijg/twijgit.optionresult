namespace TwijgIT.OptionResult {
  using System;

  using JetBrains.Annotations;

  /// <summary>
  /// Marker interface for missing value error when converting an enumerable to a single value.
  /// </summary>
  [PublicAPI]
  public struct NoElementError : IEquatable<NoElementError>, ISingleError {
    /// <summary>
    /// Determines equality to other NoElementError.
    /// </summary>
    /// <param name="other">The other NoElementError.</param>
    /// <returns>Always true.</returns>
    public bool Equals(NoElementError other) {
      return true;
    }
  }
}
