namespace TwijgIT.OptionResult {
  using System;
  using System.Collections.Generic;

  using JetBrains.Annotations;

  /// <summary>
  /// The result interface. Contains either an Ok or an Error.
  /// </summary>
  /// <typeparam name="TOk">The Ok type.</typeparam>
  /// <typeparam name="TError">The Error type.</typeparam>
  [PublicAPI]
  public interface IResult<TOk, TError> : IReadOnlyList<TOk>, IEquatable<IResult<TOk, TError>>
    where TOk : notnull where TError : notnull {
    /// <summary>
    /// Gets a value indicating whether result is Ok.
    /// </summary>
    bool IsOk { get; }

    /// <summary>
    /// Gets a value indicating whether result is Error.
    /// </summary>
    bool IsError { get; }

    /// <summary>
    /// Unwraps the Ok value.
    /// </summary>
    /// <returns>The Ok value.</returns>
    /// <exception cref="InvalidOperationException">If called on an Error.</exception>
    [NotNull]
    [Pure]
    TOk Unwrap();

    /// <summary>
    /// Uncovers the Error value.
    /// </summary>
    /// <returns>The Error value.</returns>
    /// <exception cref="InvalidOperationException">If called on an Ok.</exception>
    [NotNull]
    [Pure]
    TError Uncover();
  }
}
