namespace TwijgIT.OptionResult {
  using System;
  using System.Threading.Tasks;

  using JetBrains.Annotations;

  /// <summary>
  /// Utility methods for IResult&lt;T&gt;
  /// </summary>
  [PublicAPI]
  public static class Result {
    /// <summary>
    /// Creates an Ok value.
    /// </summary>
    /// <param name="value">The Ok value.</param>
    /// <typeparam name="TOk">The Ok value type.</typeparam>
    /// <typeparam name="TError">The Error value type.</typeparam>
    /// <returns>The Ok value.</returns>
    [ItemNotNull]
    [NotNull]
    [Pure]
    public static IResult<TOk, TError> Ok<TOk, TError>([NotNull] TOk value) where TOk : notnull where TError : notnull {
      return new Ok<TOk, TError>(value);
    }

    /// <summary>
    /// Creates an Error value.
    /// </summary>
    /// <param name="error">The Error value.</param>
    /// <typeparam name="TOk">The Ok value type.</typeparam>
    /// <typeparam name="TError">The Error value type.</typeparam>
    /// <returns>The Error value.</returns>
    [ItemNotNull]
    [NotNull]
    [Pure]
    public static IResult<TOk, TError> Error<TOk, TError>([NotNull] TError error)
      where TOk : notnull where TError : notnull {
      return new Error<TOk, TError>(error);
    }

    /// <summary>
    /// Chains the result.
    /// </summary>
    /// <param name="result">The result.</param>
    /// <param name="func">The chain function.</param>
    /// <typeparam name="TOk0">The result Ok type.</typeparam>
    /// <typeparam name="TOk1">The mapped Ok type.</typeparam>
    /// <typeparam name="TError">The error type.</typeparam>
    /// <returns>If result is Ok the return value of the chain function, the result Error otherwise.</returns>
    [ItemNotNull]
    [NotNull]
    [Pure]
    public static IResult<TOk1, TError> And<TOk0, TOk1, TError>(
      [ItemNotNull] [NotNull] this IResult<TOk0, TError> result,
      [NotNull] Func<TOk0, IResult<TOk1, TError>> func)
      where TOk0 : notnull where TOk1 : notnull where TError : notnull {
      return result.IsOk ? func(result.Unwrap()) : Error<TOk1, TError>(result.Uncover());
    }

    /// <summary>
    /// Chains the result async.
    /// </summary>
    /// <param name="result">The result.</param>
    /// <param name="func">The chain function.</param>
    /// <typeparam name="TOk0">The result Ok type.</typeparam>
    /// <typeparam name="TOk1">The mapped Ok type.</typeparam>
    /// <typeparam name="TError">The error type.</typeparam>
    /// <returns>If result is Ok the return value of the chain function, the result Error otherwise.</returns>
    [ItemNotNull]
    [NotNull]
    [Pure]
    public static async Task<IResult<TOk1, TError>> And<TOk0, TOk1, TError>(
      [ItemNotNull] [NotNull] this IResult<TOk0, TError> result,
      [NotNull] Func<TOk0, Task<IResult<TOk1, TError>>> func)
      where TOk0 : notnull where TOk1 : notnull where TError : notnull {
      return result.IsOk ? await func(result.Unwrap()) : Error<TOk1, TError>(result.Uncover());
    }

    /// <summary>
    /// Maps the Ok value.
    /// </summary>
    /// <param name="result">The result.</param>
    /// <param name="func">The mapping function.</param>
    /// <typeparam name="TOk0">The result Ok type.</typeparam>
    /// <typeparam name="TOk1">The mapped Ok type.</typeparam>
    /// <typeparam name="TError">The error type.</typeparam>
    /// <returns>The result with the Ok value mapped if present.</returns>
    [ItemNotNull]
    [NotNull]
    [Pure]
    public static IResult<TOk1, TError> SelectOk<TOk0, TOk1, TError>(
      [ItemNotNull] [NotNull] this IResult<TOk0, TError> result,
      [NotNull] Func<TOk0, TOk1> func) where TOk0 : notnull where TOk1 : notnull where TError : notnull {
      return result.IsOk ? Ok<TOk1, TError>(func(result.Unwrap())) : Error<TOk1, TError>(result.Uncover());
    }

    /// <summary>
    /// Maps the Ok value async.
    /// </summary>
    /// <param name="result">The result.</param>
    /// <param name="func">The mapping function.</param>
    /// <typeparam name="TOk0">The result Ok type.</typeparam>
    /// <typeparam name="TOk1">The mapped Ok type.</typeparam>
    /// <typeparam name="TError">The error type.</typeparam>
    /// <returns>The result with the Ok value mapped if present.</returns>
    [ItemNotNull]
    [NotNull]
    [Pure]
    public static async Task<IResult<TOk1, TError>> SelectOkAsync<TOk0, TOk1, TError>(
      [ItemNotNull] [NotNull] this IResult<TOk0, TError> result,
      [NotNull] Func<TOk0, Task<TOk1>> func) where TOk0 : notnull where TOk1 : notnull where TError : notnull {
      return result.IsOk ? Ok<TOk1, TError>(await func(result.Unwrap())) : Error<TOk1, TError>(result.Uncover());
    }

    /// <summary>
    /// Maps the Error value.
    /// </summary>
    /// <param name="result">The result.</param>
    /// <param name="func">The mapping function.</param>
    /// <typeparam name="TOk">The Ok type.</typeparam>
    /// <typeparam name="TError0">The result Error type.</typeparam>
    /// <typeparam name="TError1">The mapped Error type.</typeparam>
    /// <returns>The result with the Error value mapped if present.</returns>
    [ItemNotNull]
    [NotNull]
    [Pure]
    public static IResult<TOk, TError1> SelectError<TOk, TError0, TError1>(
      [ItemNotNull] [NotNull] this IResult<TOk, TError0> result,
      [NotNull] Func<TError0, TError1> func) where TOk : notnull where TError0 : notnull where TError1 : notnull {
      return result.IsOk ? Ok<TOk, TError1>(result.Unwrap()) : Error<TOk, TError1>(func(result.Uncover()));
    }
  }
}
