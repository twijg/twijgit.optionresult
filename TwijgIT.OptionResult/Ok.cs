namespace TwijgIT.OptionResult {
  using System;
  using System.Collections;
  using System.Collections.Generic;
  using System.Linq;

  using JetBrains.Annotations;

  /// <summary>
  /// Result containing an Ok.
  /// </summary>
  /// <typeparam name="TOk">The Ok type.</typeparam>
  /// <typeparam name="TError">The Error type.</typeparam>
  /// <remarks>A default instance of this struct should not be created.</remarks>
  [PublicAPI]
  public struct Ok<TOk, TError> : IResult<TOk, TError> where TOk : notnull where TError : notnull {
    private readonly TOk wrapped;

    internal Ok(TOk value) {
      this.wrapped = value;
      this.IsOk = true;
    }

    /// <summary>
    /// Gets an enumerator.
    /// </summary>
    /// <returns>The enumerator.</returns>
    public IEnumerator<TOk> GetEnumerator() {
      return Enumerable.Repeat(this.Unwrap(), 1).GetEnumerator();
    }

    /// <summary>
    /// Gets an enumerator.
    /// </summary>
    /// <returns>The enumerator.</returns>
    IEnumerator IEnumerable.GetEnumerator() {
      return this.GetEnumerator();
    }

    /// <summary>
    /// Gets the number of Ok elements (always 1).
    /// </summary>
    public int Count => 1;

    /// <summary>
    /// Gets an Ok element.
    /// </summary>
    /// <param name="index">The index.</param>
    /// <exception cref="IndexOutOfRangeException">If index is not zero.</exception>
    public TOk this[int index] =>
      index == 0 ? this.Unwrap() : throw new IndexOutOfRangeException($"Value of ${nameof(index)} must be 0.");

    /// <summary>
    /// Determines equality to other result.
    /// </summary>
    /// <param name="other">The other result.</param>
    /// <returns>Where other result is equal to current instance.</returns>
    public bool Equals(IResult<TOk, TError> other) {
      var thisValue = this.Unwrap();
      return other?.Any(otherValue => Equals(otherValue, thisValue)) == true;
    }

    /// <summary>
    /// Gets a value indicating whether result is Ok.
    /// </summary>
    public bool IsOk { get; }

    /// <summary>
    /// Gets a value indicating whether result is Error.
    /// </summary>
    public bool IsError => false;

    /// <summary>
    /// Unwraps the Ok value.
    /// </summary>
    /// <returns>The Ok value.</returns>
    /// <exception cref="InvalidOperationException">If called on a default instance.</exception>
    public TOk Unwrap() {
      return this.IsOk
               ? this.wrapped
               : throw new InvalidOperationException(
                   $"Cannot {nameof(this.Unwrap)} default value of {nameof(Ok<TOk, TError>)}.");
    }

    /// <summary>
    /// Uncovers the Error value.
    /// </summary>
    /// <returns>The Error value.</returns>
    /// <exception cref="InvalidOperationException">Always thrown.</exception>
    public TError Uncover() {
      throw new InvalidOperationException($"Cannot {nameof(this.Uncover)} {nameof(Ok<TOk, TError>)}.");
    }

    /// <summary>
    /// Gets a string representation of the current instance.
    /// </summary>
    /// <returns>A string representation of the current instance.</returns>
    public override string ToString() {
      return this.Unwrap().ToString();
    }

    /// <summary>
    /// Deconstructs the Ok.
    /// </summary>
    /// <param name="value">The value.</param>
    public void Deconstruct(out TOk value) {
      value = this.Unwrap();
    }

    /// <summary>
    /// Converts Some to the wrapped value.
    /// </summary>
    /// <param name="ok">An instance of Ok.</param>
    /// <returns>The wrapped value.</returns>
    public static implicit operator TOk(Ok<TOk, TError> ok) {
      return ok.Unwrap();
    }

    /// <summary>
    /// Wraps a value in the Ok struct.
    /// </summary>
    /// <param name="value">The value.</param>
    /// <returns>An instance of Ok wrapping the value.</returns>
    public static implicit operator Ok<TOk, TError>(TOk value) {
      return new Ok<TOk, TError>(value);
    }
  }
}
